#!/bin/bash

WA="YOUR_DISK_IS_FULL"

declare -i ALERT=10

df -H | awk '{ print $5 " " $1 }' | while read output; do
 
declare -i usep=$(echo $output | awk '{ print $1}' | cut -d'%' -f1 ) 

partition=$(echo $output | awk '{ print $2 }' )

if [ "$usep" -ge "$ALERT" ] ; then

  echo  "Alert: Almost out of disk space $usep% $partition"

  echo "$WA"

fi

echo "$partition =====================> $usep %"
 
done
